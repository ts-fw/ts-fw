import {Container} from 'inversify';

const expressApplicationContainer: Container = new Container({defaultScope: 'Singleton'});

export abstract class ApplicationContainerProvider {
    static get(): Container {
        return expressApplicationContainer;
    }
}
