export interface IConfigStorage {
    load(config: any);
    get<T extends any>(key: string, defaultValue?: any): T;
}
