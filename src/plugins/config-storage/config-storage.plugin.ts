import {IConfigStorage} from './config-storage.interface';
import * as dotProp from 'dot-prop';

export class ConfigStorage implements IConfigStorage {
    private _config: any = {};

    load(config) {
        this._config = config;
    }

    get<T>(key: string, defaultValue?: any): T {
        return dotProp.get(this._config, key, defaultValue);
    }
}
