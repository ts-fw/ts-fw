import {Provider} from '../decorators';
import axios, {AxiosInstance} from 'axios';

@Provider('httpClient')
export class HttpClientProvider {
    provide(): AxiosInstance {
        return axios.create();
    }
}
