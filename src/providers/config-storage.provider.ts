import {Provider} from '../decorators';
import {IConfigStorage, ConfigStorage} from '../plugins';

@Provider('configStorage')
export class ConfigStorageProvider {
    provide(): IConfigStorage {
        return new ConfigStorage();
    }
}
