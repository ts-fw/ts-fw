import {Provider} from '../decorators';
import * as winston from 'winston';

@Provider('logger')
export class LoggerProvider {
    provide(): winston {
        return new winston.Logger();
    }
}
