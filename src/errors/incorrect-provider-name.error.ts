import {ApplicationError} from './application-error.abstract';

export class IncorrectProviderError extends ApplicationError {
    constructor() {
        super(IncorrectProviderError, 'Provider method must be have a name for provide dependency');
    }
}
