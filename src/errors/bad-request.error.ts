import {APIError} from './api-error.base';

export class BadRequestError extends APIError {
    constructor(_message: string) {
        super(BadRequestError, _message, 400);
    }
}
