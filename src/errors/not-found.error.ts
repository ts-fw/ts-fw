import {APIError} from './api-error.base';

export class NotFoundError extends APIError {
    constructor(_message: string) {
        super(NotFoundError, _message, 404);
    }
}
