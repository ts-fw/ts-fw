import {APIError} from './api-error.base';

export class ForbiddenError extends APIError {
    constructor(_message: string) {
        super(ForbiddenError, _message, 403);
    }
}
