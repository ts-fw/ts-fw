import {APIError} from './api-error.base';

export class UnauthorizedError extends APIError {
    constructor(_message: string) {
        super(UnauthorizedError, _message, 401);
    }
}
