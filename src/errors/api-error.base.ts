import {ApplicationError} from './application-error.abstract';

export abstract class APIError extends ApplicationError {
    constructor(clazz: Function, message: string, status: number = 500, public details?: string) {
        super(clazz, message, status);
    }
}
