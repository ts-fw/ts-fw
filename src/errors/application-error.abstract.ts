export abstract class ApplicationError extends Error {
    constructor(clazz: Function, public message: string, public status: number = 500) {
        super();
        this.name = clazz.name;
        Error.captureStackTrace(this, clazz);
    }
}
