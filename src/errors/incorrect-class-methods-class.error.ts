import {ApplicationError} from './application-error.abstract';

export class IncorrectClassMethodsError extends ApplicationError {
    constructor() {
        super(IncorrectClassMethodsError, 'Class must be have one public method, not constructor.');
    }
}

