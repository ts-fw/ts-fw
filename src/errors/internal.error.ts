import {APIError} from './api-error.base';

export class InternalError extends APIError {
    constructor(message: string) {
        super(InternalError, message, 500);
    }
}
