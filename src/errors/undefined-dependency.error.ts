import {ApplicationError} from './application-error.abstract';

export class UndefinedDependencyError extends ApplicationError {
    constructor() {
        super(UndefinedDependencyError, 'Provider method must be return value');
    }
}
