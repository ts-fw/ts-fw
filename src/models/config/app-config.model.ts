export interface IAppConfigModel {
    port: number;
    debug: boolean;
}
