export interface ILoggerConfigModel {
    transports: ITransportConfigModel[];
}

export interface ITransportConfigModel {
    level: 'error' | 'warn' | 'info' | 'verbose' | 'debug' | 'silly';
    type: 'console' | 'file';
    directoryName?: string;
    fileName?: string;
    json?: boolean;
}
