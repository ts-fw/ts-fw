import * as express from 'express';
import {InversifyExpressServer} from 'inversify-express-utils';
import {ApplicationContainerProvider} from '../ioc';
import {Container} from 'inversify';
import {Config, HTTP} from '../decorators';
import * as winston from 'winston';
import {IConfigStorage} from '../plugins';
import {EConfig} from '../enums';
import {UndefinedDependencyError, IncorrectProviderError} from '../errors';
import {IAppConfigModel} from '../models';
import Application = express.Application;

@HTTP('/')
class DefaultTemplate {
}

export class ExpressServerApplication extends InversifyExpressServer {
    constructor() {
        super(ApplicationContainerProvider.get());
    }

    build(): Application {
        const container: Container = ApplicationContainerProvider.get();
        container.bind('di').toConstantValue(container);

        for (let index = 1; ; index++) {
            const providers: any[] = [];
            try {
                const tempProviders = container.getAllNamed('Provider', `${index}`);
                providers.push(...tempProviders);
            } catch (ignore) {
                break;
            }

            for (const provider of providers) {
                const method: string = Reflect.getMetadata('method', provider);
                const providedInjectableName: string = Reflect.getMetadata('name', provider);
                if (!providedInjectableName || providedInjectableName === '') {
                    throw new IncorrectProviderError();
                }

                try {
                    const injectable = provider[method]();
                    if (!injectable || injectable === null) {
                        throw new UndefinedDependencyError();

                    }

                    container.bind(providedInjectableName).toConstantValue(injectable);
                } catch (err) {
                    if (err.name === 'UndefinedDependencyError') {
                        throw err;
                    }
                }
            }
        }

        const logger = container.get<winston.Winston>('logger');

        this.setConfig((application: Application) => {
            for (let index = 1; ; index++) {
                const configurators: any[] = [];
                try {
                    const tempConfigs = container.getAllNamed('Config', `${index}`);
                    configurators.push(...tempConfigs);
                } catch (ignore) {
                    break;
                }

                for (const configurator of configurators) {
                    try {
                        const type: EConfig = Reflect.getMetadata('type', configurator);
                        const method: string = Reflect.getMetadata('method', configurator);
                        if (type === EConfig.Application) {
                            configurator[method](application);
                        }
                    } catch (error) {
                        logger.error(error);
                    }
                }
            }
        });

        this.setErrorConfig((application: Application) => {
            const errorHandlers: any[] = container.getAll('ErrorHandler');
            const errorHandler: any = errorHandlers[errorHandlers.length > 1 ? errorHandlers.length - 1 : 0];
            const method: string = Reflect.getMetadata('method', errorHandler);
            application.use((error, req, res, next) => {
                errorHandler[method](error, req, res, next);
            });
        });

        const application = super.build();

        const configStorage = container.get<IConfigStorage>('configStorage');
        const appConfig = configStorage.get<IAppConfigModel>('app') || <IAppConfigModel>{};

        container.bind('express').toConstantValue(application);

        const port = appConfig.port || 3030;
        const server = application.listen(port, () => {
            logger.info(`Application started on ${server.address().address}:${server.address().port}`);
        });

        const configurators: any[] = container.getAll('Config');
        for (const configurator of configurators) {
            try {
                const type: EConfig = Reflect.getMetadata('type', configurator);
                const method: string = Reflect.getMetadata('method', configurator);
                if (type === EConfig.Server) {
                    configurator[method](server);
                }
            } catch (error) {
                logger.error(error);
            }
        }
        container.bind('server').toConstantValue(server);

        return application;
    }
}
