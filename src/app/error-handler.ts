import {ErrorHandler} from '../decorators';
import {inject} from 'inversify';
import {IConfigStorage} from '../plugins';
import * as winston from 'winston';
import * as express from 'express';
import {ApplicationError} from '../errors';
import {IAppConfigModel} from '../models';

@ErrorHandler()
export class AppErrorHandler {
    @inject('configStorage')
    private configStorage: IConfigStorage;

    @inject('logger')
    private logger: winston.Winston;

    handleError(error: ApplicationError, ignore: express.Request, res: express.Response) {
        const config = this.configStorage.get<IAppConfigModel>('app');
        if (config.debug) {
            this.logger.error(error.stack);
        }

        this.logger.error(error);
        res.status(error.status || 500).json(error);
    }
}
