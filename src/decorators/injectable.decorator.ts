import {makeProvideDecorator, makeFluentProvideDecorator} from 'inversify-binding-decorators';
import {ApplicationContainerProvider} from '../ioc';

const Injectable = <any>makeProvideDecorator(ApplicationContainerProvider.get());
const fluentProvider: Function = makeFluentProvideDecorator(ApplicationContainerProvider.get());
const InjectableWithName: Function = (identifier: any, name: string) => {
    return fluentProvider(identifier)
        .whenTargetNamed(name)
        .done();
};

export {Injectable, InjectableWithName};
