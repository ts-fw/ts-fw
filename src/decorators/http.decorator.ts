import {InjectableWithName} from './injectable.decorator';
import {controller, TYPE} from 'inversify-express-utils';

export function HTTP(path: string, name?: string): ClassDecorator {
    return (constructor: Function) => {
        Reflect.decorate([
            InjectableWithName(TYPE.Controller, name || constructor.name), controller(path)
        ], constructor);
    }
}
