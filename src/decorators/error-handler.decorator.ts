import {InjectableWithName} from './injectable.decorator';
import {ReflectUtils} from '../utils';

export function ErrorHandler(): ClassDecorator {
    return (constructor: Function) => {
        Reflect.decorate([InjectableWithName('ErrorHandler')], constructor);
        ReflectUtils.annotateMethod(constructor);
    }
}
