import {EConfig} from '../enums';
import {InjectableWithName} from './injectable.decorator';
import {ReflectUtils} from '../utils';

export function Config(priority: number = 1, type: EConfig = EConfig.Application): ClassDecorator {
    return (constructor: Function) => {
        Reflect.decorate([InjectableWithName('Config', String(priority))], constructor);
        Reflect.defineMetadata('type', type, constructor.prototype);
        ReflectUtils.annotateMethod(constructor);
    }
}
