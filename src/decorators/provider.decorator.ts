import {InjectableWithName} from './injectable.decorator';
import {ReflectUtils} from '../utils';

export function Provider(name: string, priority: number = 1): ClassDecorator {
    return (constructor: Function) => {
        Reflect.decorate([InjectableWithName('Provider', String(priority || 1))], constructor);
        Reflect.defineMetadata('name', name, constructor.prototype);
        ReflectUtils.annotateMethod(constructor);
    }
}
