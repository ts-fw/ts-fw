export function Method(): MethodDecorator {
    return (constructor: any, propertyKey: string) => {
        Reflect.defineMetadata('method', propertyKey, constructor);
    }
}
