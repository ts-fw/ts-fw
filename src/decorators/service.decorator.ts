import {Injectable} from './injectable.decorator';

export function Service(name?: string): ClassDecorator {
    return (constructor: Function) => {
        if (!name) {
            name = constructor.name.charAt(0).toLowerCase() + constructor.name.slice(1);
        }

        Reflect.decorate([Injectable(name)], constructor);
    }
}
