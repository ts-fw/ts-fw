import getDecorators from 'inversify-inject-decorators';
import {ApplicationContainerProvider} from '../ioc';

export const {
    lazyInject,
    lazyInjectNamed
} = <any>getDecorators(ApplicationContainerProvider.get());
