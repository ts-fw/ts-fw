import {IncorrectClassMethodsError} from '../errors';

export abstract class ReflectUtils {
    static annotateMethod(constructor: Function) {
        try {
            const method = ReflectUtils._findMethod(constructor);
            Reflect.defineMetadata('method', method, constructor.prototype);
        } catch (err) {
            console.error(err);
        }
    }

    private static _findMethod(constructor: Function): string {
        const methodsArray: string[] = [];

        for (const method in constructor.prototype) {
            if (method.indexOf('_') === 0 || method === 'constructor') {
                continue;
            }

            const descriptor = Object.getOwnPropertyDescriptor(constructor.prototype, method);

            if (descriptor.get) {
                continue;
            }

            if (typeof constructor.prototype[method] !== 'function') {
                continue;
            }

            methodsArray.push(method);
        }

        if (methodsArray.length > 1 || methodsArray.length === 0) {
            throw new IncorrectClassMethodsError();
        }

        return methodsArray[0];
    }
}
