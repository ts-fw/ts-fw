import {Config} from '../decorators';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import Application = express.Application;

@Config()
export class BodyParserConfig {
    configure(app: Application) {
        app.use(bodyParser.urlencoded({
            extended: true
        }));
        app.use(bodyParser.json())
    }
}
