import {Config} from '../decorators';
import * as express from 'express';
import * as helmet from 'helmet';
import Application = express.Application;

@Config()
export class HelmetConfig {
    configure(app: Application) {
        app.use(helmet());
    }
}
