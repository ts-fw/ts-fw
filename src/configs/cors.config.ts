import {inject} from 'inversify';
import * as express from 'express';
import * as cors from 'cors';
import {Config} from '../decorators';
import {IConfigStorage} from '../plugins/config-storage';
import {IAppConfigModel} from '../models/config';
import Application = express.Application;

@Config(2)
export class CorsConfig {
    @inject('configStorage')
    private configStorage: IConfigStorage;

    configure(app: Application) {
        const appConfig = this.configStorage.get<IAppConfigModel>('app', {});
        if (appConfig.debug) {
            app.use(cors());
        }
    }
}
