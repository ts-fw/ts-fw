import * as winston from 'winston';
import {inject} from 'inversify';
import * as moment from 'moment';
import {Config} from '../decorators';
import {IConfigStorage} from '../plugins';
import {ITransportConfigModel, ILoggerConfigModel} from '../models';

@Config(2)
export class LoggerConfig {
    @inject('configStorage')
    private configStorage: IConfigStorage;

    @inject('logger')
    private logger: winston;

    configure() {
        const loggerConfig = this.configStorage.get<ILoggerConfigModel>('logger', {
            transports: <ITransportConfigModel[]>[{
                level: 'info',
                type: 'console'
            }]
        });

        for (const transport of loggerConfig.transports) {
            const level = transport.level;
            const type = transport.type;

            switch (type) {
                case 'file': {
                    const isJSON = transport.json;
                    const directoryName = transport.directoryName;
                    const fileName = transport.fileName;
                    const filePath = directoryName + '/' + fileName;
                    this.logger.add(winston.transports.File, {
                        level: level,
                        filename: filePath,
                        formatter: LoggerConfig._formatter,
                        json: isJSON,
                        options: {
                            flags: 'w+'
                        }
                    });
                    break;
                }
                default: {
                    this.logger.add(winston.transports.Console, {
                        level: level,
                        formatter: LoggerConfig._formatter
                    });
                    break;
                }
            }
        }
    }

    private static _formatter(args: any) {
        const dateObject = new Date();
        const date: string = moment(dateObject).format('DD/MM/YYYY-HH:mm:ss');
        let logFormat = `${date} - ${args.level} - ${args.message}`;

        if (args.meta && Object.keys(args.meta).length !== 0) {
            logFormat += ` - ${JSON.stringify(args.meta)}`;
        }

        return logFormat;
    }
}
