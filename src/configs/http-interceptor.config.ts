import {Config} from '../decorators';
import {inject} from 'inversify';
import {AxiosInstance, AxiosResponse, AxiosError} from 'axios';
import {APIError, BadRequestError, UnauthorizedError, ForbiddenError, InternalError} from '../errors';

@Config()
export class HttpInterceptorConfig {
    @inject('httpClient')
    private httpClient: AxiosInstance;

    configure() {
        this.httpClient.interceptors.response.use(
            HttpInterceptorConfig._onResponseComplete.bind(this),
            HttpInterceptorConfig._onResponseError.bind(this)
        );
    }

    private static _onResponseComplete(response: AxiosResponse): AxiosResponse {
        return response;
    }

    private static async _onResponseError(error: AxiosError): Promise<APIError> {
        let apiError: APIError;
        switch (error.code) {
            case '400': {
                apiError = new BadRequestError(error.response['error'].message);
                break;
            }
            case '401': {
                apiError = new UnauthorizedError(error.response['error'].message);
                break;
            }
            case '403': {
                apiError = new ForbiddenError(error.response['error'].message);
                break;
            }
            case '404': {
                apiError = new ForbiddenError(error.response['error'].message);
                break;
            }
            default: {
                apiError = new InternalError(error.response['error'].message);
                break;
            }
        }

        return apiError;
    }
}
