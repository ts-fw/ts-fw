import {Config} from '../decorators';
import * as express from 'express';
import * as morgan from 'morgan';
import Application = express.Application;

@Config()
export class MorganConfig {
    configure(app: Application) {
        app.use(morgan('combined'));
    }
}
